# Search by current language

This module will remove the options to filter results by language and
always show the current language results in the core node search.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/search_current_language).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search_current_language).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration.


# MAINTAINERS

Current maintainers:
- João Mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)
